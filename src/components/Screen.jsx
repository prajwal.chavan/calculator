import React, { Component } from 'react';
import Result from './Result';
import Log from './Log';

export default class Screen extends Component {
  render() {
    return (
      <div className="font-['Orbitron'] text-right mb-2 mr-2 ml-2 p-2 text-xl bg-[#b2c2b3]">
        <Log log={this.props.log} handleLogChange={this.props.handleLogChange} />
        <Result result={this.props.result} />
      </div>
    );
  }
}