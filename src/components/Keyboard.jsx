import React, { Component } from 'react';
import Key from './Key';

export default class Keyboard extends Component {
  render() {
  // for better readability of Key components - shorter lines
    const keyClick = this.props.keyClick;
    const x = <span>&#x1D4B3;</span>;
    const inv = <sup>-1</sup>;
    const pow2 = <sup>2</sup>;
    const pow3 = <sup>3</sup>;

    return (
      <div className="mr-2 mb-2 ml-2">
        <div>
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" Tag={[x, inv]} keyLog="^-1" math="inv" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" Tag="&radic;" keyLog="&radic;(" math="sqrt" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" Tag={[x, pow2]} keyLog="^2" math="sqr" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" Tag="^" keyLog="^" math="power" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" Tag="log" keyLog="log(" math="log" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" Tag="ln" keyLog="ln(" math="log" keyClick={keyClick} />
        </div>
        <div>
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="-" Tag="(&minus;)" math="sub" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="^3" Tag={[x, pow3]} math="power" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="" Tag="hyp" math="log" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="sin(" Tag="sin" math="trig" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="cos(" Tag="cos" math="trig" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="tan(" Tag="tan" math="trig" keyClick={keyClick} />
        </div>
        <div>
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="" Tag="RCL" math="" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="" Tag="ENG" math="" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="(" Tag="(" math="prnths" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog=")" Tag=")" math="prnths" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="" Tag="," math="log" keyClick={keyClick} />
          <Key className="bg-[#202742] w-9 mt-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[11px] rounded-tr-[11p] rounded-br-[11px] rounded-bl-[11px]" keyLog="" Tag="M+" math="log" keyClick={keyClick} />
        </div>
        <div>
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="1" keyLog="1" math="number" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="2" keyLog="2" math="number" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="3" keyLog="3" math="number" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="DEL" math="delete" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-12 mt-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="AC" math="clear" keyClick={keyClick} />
        </div>
        <div>
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="4" keyLog="4" math="number" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="5" keyLog="5" math="number"keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="6" keyLog="6" math="number"keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="&times;" keyLog="&times;" math="multiply" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-12 mt-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="&divide;" keyLog="&divide;" math="divide" keyClick={keyClick} />
        </div>
        <div>
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="7" keyLog="7" math="number" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="8" keyLog="8" math="number" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="9" keyLog="9" math="number" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="+" keyLog="+" math="sum" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-12 mt-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="&minus;" keyLog="-" math="subtract" keyClick={keyClick} />
        </div>
        <div>
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="0" keyLog="0" math="log" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="." keyLog="." math="comma" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="EXP" keyLog="E" math="exponent" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-11 mt-2 mr-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="Ans" keyLog="Ans" math="ans" keyClick={keyClick} />
          <Key className="bg-[#e45353] w-12 mt-2 text-white p-1 cursor-pointer font-['Verdana'] text-[#fff] text-sm border-none cursor-pointer outline-none
        rounded-tl-[10px] rounded-tr-[10px] rounded-br-[12px] rounded-bl-[12px]" Tag="=" math="equals" keyClick={keyClick} />
        </div>
      </div>
    );
  }
}
