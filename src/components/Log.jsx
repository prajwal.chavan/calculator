import React, { Component } from 'react';

export default class Log extends Component {
  constructor(props) {
    super(props);
    this.handleLogChange = this.handleLogChange.bind(this);
  }

  componentDidMount() {
    window.addEventListener('click', () => this.ref.focus());
  }

  componentDidUpdate() {
    this.ref.focus();
  }

  handleLogChange() {
    this.props.handleLogChange(this.ref.value);
  }
  
  render() {
    return (
      <input
        type="text"
        className="bg-[#b2c2b3] font-['Orbitron'] h-6 outline-none border-none text-sm text-left w-full"
        ref={(a) => { this.ref = a; }}
        value={this.props.log}
        onChange={this.handleLogChange}
        // autoFocus
        inputmode = "none"
      />
    );
  }
}